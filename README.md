# Bash script to sort files




### Description

The following script sorts your images into directories based on the date the photo was modified. 
The first argument is for the image path directory and the second for the file type

Files are sorted this way in directories:

```
Year
    Month
        Day
            File.JPG
```
            
            

#### Example

```console
user@pc:~$ . export.sh OLYMPUS/ JPG
```


```    
2019
    01
        14
            photo22.JPG
            photo41.JPG
        
        31
            photo1.JPG
            photo456.JPG
        
        
    11
        15
            photo4.JPG
            
2012
    01
        02
            photo3.JPG

```

## Tools

Bash interpreter


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

