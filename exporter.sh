#!/bin/sh

cd $1 #$1 est l'arg. du repertoire source, $2 est l'arg. du type de fichier a trier
for var in *
do
  if [[ $var == *".$2" ]]; then
    datetemp=$(stat -c %y "$var" | awk '{print $1}')

    annee=$(echo $datetemp | awk -F "-" '{print $1}')
    mois=$(echo $datetemp | awk -F "-" '{print $2}')
    jour=$(echo $datetemp | awk -F "-" '{print $3}')

    mkdir $annee -p
    cd $annee
    mkdir $mois -p
    cd $mois
    mkdir $jour -p
    cd $jour
    cp ../../../$var .
    echo "$var"
    cd ../../..

  fi
done
